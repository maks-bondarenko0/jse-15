package ru.t1.bondarenko.tm.command.system;

import ru.t1.bondarenko.tm.api.service.ICommandService;
import ru.t1.bondarenko.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
