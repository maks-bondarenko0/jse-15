package ru.t1.bondarenko.tm.command.task;

import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Start Task by index.";

    public final static String NAME = "task-start-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }
}
