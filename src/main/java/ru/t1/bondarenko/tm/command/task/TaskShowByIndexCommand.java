package ru.t1.bondarenko.tm.command.task;

import ru.t1.bondarenko.tm.model.Task;
import ru.t1.bondarenko.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Show Task by Index.";

    public final static String NAME = "task-show-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findByIndex(index);
        showTask(task);
    }
}
