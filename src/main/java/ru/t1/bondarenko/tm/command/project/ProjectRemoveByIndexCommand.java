package ru.t1.bondarenko.tm.command.project;

import ru.t1.bondarenko.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Delete Project by index.";

    public final static String NAME = "project-delete-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().removeByIndex(index);
    }
}
