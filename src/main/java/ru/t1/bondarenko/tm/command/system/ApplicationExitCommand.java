package ru.t1.bondarenko.tm.command.system;

public class ApplicationExitCommand extends AbstractSystemCommand {

    public final static String DESCRIPTION = "Terminate the application.";

    public final static String NAME = "exit";

    public final static String ARGUMENT = null;

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
