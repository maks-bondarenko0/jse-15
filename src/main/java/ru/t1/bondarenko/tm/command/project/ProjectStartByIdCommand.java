package ru.t1.bondarenko.tm.command.project;

import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Start Project by ID.";

    public final static String NAME = "project-start-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.IN_PROGRESS);
    }
}
