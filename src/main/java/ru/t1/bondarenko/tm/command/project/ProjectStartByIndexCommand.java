package ru.t1.bondarenko.tm.command.project;

import ru.t1.bondarenko.tm.enumerated.Status;
import ru.t1.bondarenko.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    public final static String DESCRIPTION = "Start Project by index.";

    public final static String NAME = "project-start-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }
}
