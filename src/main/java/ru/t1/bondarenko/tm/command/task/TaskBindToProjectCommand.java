package ru.t1.bondarenko.tm.command.task;

import ru.t1.bondarenko.tm.util.TerminalUtil;

public class TaskBindToProjectCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Bind Task to Project.";

    public final static String NAME = "task-bind-to-project";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter Project ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter Task ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
    }
}
