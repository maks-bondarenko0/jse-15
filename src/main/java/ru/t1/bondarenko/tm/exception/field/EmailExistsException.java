package ru.t1.bondarenko.tm.exception.field;

public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}